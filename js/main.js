var removeThenAdd = function removeThenAdd(removeEl, AddEl, className) {
	removeEl.classList.remove(className);
	AddEl.classList.add(className);
};

// https://coderwall.com/p/i817wa/one-line-function-to-detect-mobile-devices-with-javascript
var isMobileDevice = function isMobileDevice() {
	return typeof window.orientation !== "undefined" || navigator.userAgent.indexOf('IEMobile') !== -1;
};

var popupSubscribe = get.el(".popup.subscribe");
var openSubscribe = get.all(".open-subscribe");
var closeSubscribe = get.all(".close-subscribe");

var popupWechat = get.el(".popup.wechat");
var openWechat = get.all(".open-wechat");
var closeWechat = get.all(".close-wechat");

openSubscribe.forEach(function (open) {
	open.onclick = function (e) {
		e.preventDefault();
		popupSubscribe.classList.add("show");
	};
});

closeSubscribe.forEach(function (close) {
	close.onclick = function () {
		popupSubscribe.classList.remove("show");
	};
});

// if desktop
if (!isMobileDevice()) {
	openWechat.forEach(function (open) {
		open.onclick = function (e) {
			e.preventDefault();
			popupWechat.classList.add("show");
		};
	});

	closeWechat.forEach(function (close) {
		close.onclick = function () {
			popupWechat.classList.remove("show");
		};
	});
}

var body = document.body;
body.onkeyup = function (e) {
	if (e.keyCode === 27) {
		popupSubscribe.classList.remove("show");
		popupWechat.classList.remove("show");
	}
};

// tab
var tabs = get.allArray(".tab");

tabs.forEach(function(tab){

	var tabButtonsWrapper = tab.querySelector(".tab-buttons-wrapper");
	var tabButtons = tab.querySelectorAll(".tab-buttons-wrapper a");

	tabButtons = [].slice.call(tabButtons);

	var tabContentsWrapper = tab.querySelector(".tab-contents-wrapper");
	var tabContents = tab.querySelectorAll(".tab-contents-wrapper .content");

	tabContents = [].slice.call(tabContents);

	tabButtons.forEach(function (theTab) {
		theTab.onclick = function (e) {
			e.preventDefault();
			var index = tabButtons.indexOf(this);
			var activeTab = tabButtonsWrapper.querySelector(".active");

			var activeContent = tabContentsWrapper.querySelector(".active");
			var thisContent = tabContents[index];

			removeThenAdd(activeTab, this, "active");

			removeThenAdd(activeContent, thisContent, "active");
		};
	});

});

var format = 'hh:mm a';
var stores = ["newyork", "london", "hongkong"];

var worldTimes = {
	"newyork": moment.tz("America/New_York").format(format),
	"london": moment.tz("Europe/London").format(format),
	"hongkong": moment.tz("Asia/Hong_Kong").format(format)
}

// var openStores = {
// 	"newyork": ["10:00 am", "06:00 pm"],
// 	"london": ["10:00 am", "06:00 pm"],
// 	"hongkong": ["11:00 am", "07:00 pm"]
// }

var openStores = {
	"newyork": "10:00 am",
	"london": "10:00 am",
	"hongkong": "11:00 am" 
}

var closedStores = {
	"newyork": "06:00 pm",
	"london": "06:00 pm",
	"hongkong": "07:00 pm"
}

stores.forEach(function(store){

	var beforeTime = openStores[store].toString();
	var afterTime = closedStores[store].toString();

	var currentTime = moment(worldTimes[store], format);
	var momentBeforeTime = moment(beforeTime, format);
	var momentAfterTime = moment(afterTime, format);

	var openHours = get.all(".open-hours-" + store);

	if(!currentTime.isBetween(momentBeforeTime, momentAfterTime)) {

		openHours.forEach(function(openHour){
		  console.log(store, 'is currently closed')
		  openHour.textContent = "暫停營業";
		});

	}

});

var shareButton = get.el(".share-button");
var socialButtons = get.all(".social-button");

shareButton.onclick = function () {
	socialButtons.forEach(function (button) {
		button.classList.toggle("show");
	});
};