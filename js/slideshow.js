"use strict";

var zwirnerSlideshows = get.all(".zwirner-slideshow");

zwirnerSlideshows.forEach(function (slideshow) {

	var slides = slideshow.querySelectorAll(".slide");
	slides = [].slice.call(slides);

	var contents = slideshow.querySelectorAll(".content");
	contents = [].slice.call(contents);

	var nextControl = slideshow.querySelector(".control.next");
	var prevControl = slideshow.querySelector(".control.prev");

	var currentSlide = void 0;

	nextControl.addEventListener("click", function () {

		currentSlide = slideshow.querySelector(".active");
		var nextSlide = currentSlide.nextElementSibling;
		var lastSlide = nextSlide.nextElementSibling;

		var currentIndex = slides.indexOf(currentSlide);
		var nextIndex = slides.indexOf(nextSlide);

		if (nextSlide !== null) {
			removeThenAdd(currentSlide, nextSlide, "active");

			if (!slideshow.classList.contains("no-content")) {
				removeThenAdd(contents[currentIndex], contents[nextIndex], "active");
			}
		}

		if (lastSlide === null) {
			this.classList.add("disabled");
			prevControl.classList.remove("disabled");
		} else {
			prevControl.classList.remove("disabled");
		}
	});

	prevControl.addEventListener("click", function () {

		currentSlide = slideshow.querySelector(".active");
		var prevSlide = currentSlide.previousElementSibling;
		var lastSlide = prevSlide.previousElementSibling;

		var currentIndex = slides.indexOf(currentSlide);
		var prevIndex = slides.indexOf(prevSlide);

		if (prevSlide !== null) {
			removeThenAdd(currentSlide, prevSlide, "active");

			if (!slideshow.classList.contains("no-content")) {
				removeThenAdd(contents[currentIndex], contents[prevIndex], "active");
			}
		}

		if (lastSlide === null) {
			this.classList.add("disabled");
			nextControl.classList.remove("disabled");
		} else {
			nextControl.classList.remove("disabled");
		}
	});
});

// const removeThenAdd = (removeEl, AddEl, className) => {
// 	removeEl.classList.remove(className);
// 	AddEl.classList.add(className);
// }


// news slides
var newsSlide = get.el(".news-slides");
var newsSlides = get.all(".news-slides .slide");
var slidesScroller = get.el(".news-slides-scroller");
var slideMargin = parseInt(window.getComputedStyle(newsSlides[0]).marginRight);
var slideWidth = newsSlides[0].clientWidth + slideMargin;

var slidesPos = [];

for (var i = 1; i < newsSlides.length; i++) {
	slidesPos.push((slideWidth - 1) * i);
}

console.log(slidesPos);

var nextControl = get.el("#news .next");
nextControl.state = "next";
var prevControl = get.el("#news .prev");
prevControl.state = "prev";

var currentPos = 0;
var currentSlide = 0;

// nextControl.onclick = ()=> {

// 	currentPos = slidesScroller.scrollLeft;

// 	const nextPos = slidesPos.filter((pos)=> {
// 		return currentPos > pos;
// 	}).length + 1;

// 	console.log(nextPos);

// 	anime({
// 		targets: slidesScroller,
// 		scrollLeft: nextPos * slideWidth,
// 		easing: 'easeOutQuad',
// 		duration: 300
// 	})
// }

var controlit = function controlit(prevOrNext) {

	prevOrNext.onclick = function () {
		var _this = this;

		this.classList.add("disabled");

		currentPos = slidesScroller.scrollLeft;

		var prevOrNextPos = slidesPos.filter(function (pos) {
			return currentPos > pos;
		}).length;

		var customEase = void 0;

		if (prevOrNext.state == "next") {
			prevOrNextPos += 1;
			customEase = "easeOutQuad";
		}

		if (prevOrNext.state == "prev") {
			prevOrNextPos -= 1;
			customEase = "easeInQuad";
		}

		anime({
			targets: slidesScroller,
			scrollLeft: prevOrNextPos * slideWidth,
			easing: "linear",
			duration: 300,
			complete: function complete() {
				_this.classList.remove("disabled");
			}
		});
	};
};

controlit(nextControl);
controlit(prevControl);

newsSlide.style.width = slideWidth * newsSlides.length - slideMargin + "px";